                      

                Roasted Butternut Squash Smash with Pork Ideas
Serving Size  : 8 – 12…    
                        Roasted Squash Smash
  1               pint  heavy cream, Sassy Cow
  6             cloves  garlic -- Peeled and slowly simmered in Heavy Cream
                     
  1               each  squash, butternut -- Washed well, Split in half down the center, lengthwise, Deseeded,
  1         tablespoon  olive oil, extra virgin -- rub oil into the skin before roasting
                       
     1/2         stick  butter,  unsalted
  3        tablespoons  Honey -- The market offers great honey!
  1           teaspoon  kosher salt
     1/4      teaspoon  black pepper -- freshly ground if you can
			
                        Roasted Pork Idea:
                        BRINE
  2               cups  cold water
  4        tablespoons  kosher salt
  4        tablespoons  sugar
  1           teaspoon  juniper berries -- rough ground or use an electric coffee bean grinder
  1           teaspoon  black peppercorn -- rough ground or use an electric coffee bean grinder
     1/2      teaspoon  allspice, whole -- rough ground or use an electric coffee bean grinder
  2               each  bay leaf
     1/2          each  Onion -- medium diced
  3             cloves  garlic
                        

PORK
  1              pound  pork -- You can use whatever cut you like. Tenderloin, Loin, Pork Chop, Shoulder, whatever

Roasted Squash Mash:
Preheat oven to 375F
Begin by placing the cream into a medium sauce pan, add peeled garlic bulbs and begin reducing + infusing over very low heat. Be careful because the cream can boil over easily if over too high of heat. We want it to reduce by cooking down, not by boiling over! Allow the garlic to cook slowly in the cream until it becomes soft and begins to brown a bit. Fresh Wisconsin Garlic is truly wonderful. Ask if anyone has any garlic hanging around. 
Thoroughly wash the skin of squash. Slice the ends off of the squash. Carefully cut the squash down the center, lengthwise, from stem to end, through the seed pocket. Scrape away the seeds and place skin side up onto an oiled sheet pan, (you could place foil or parchment paper down to ease clean up). Rub the skin with oil and begin patiently roasting in your 375F oven. This recipe would likely use a whole medium sized butternut squash, so you can judge how many people you trying to feed. Allow the squash to roast for 30 to 40 minutes, it will be soft once it is ready and you will want to allow it to cool a bit before handling for the mash. Once the squash is ready to be scooped, carefully begin peeling the skin away or use a spoon to scrape the flesh into a large mixing bowl. If you have a raised cooling rack (like for cookies!) you can try placing the rack over the bowl, then pushing the flesh through the rack grate. You will be left with skin at the surface as the flesh passes though. This is a cool tip for soft vegetables like these!
By now your garlic cream should be nice and toasty with the soft garlic. You can whisk the butter into the garlic cream, then begin stirring the cream, honey, and seasoning to the butternut smash. Stir until the squash is tasty, adjusting the season to your liking. Our Wisconsin Honey is super good and the market will have different sizes for you this week! 
Hold the squash smash warm.
Roasted Pork Ideas:
We do a lot of brining and marinating in Cassiopeia. Brining is an easy way to inject flavor and moisture into meats that you like. This is a simple steeped brine that you can use with Pork or Chicken.
Brine:
Bring the Brine ingredients to a simmer in a medium sauce pan. Once simmering, allow it to steep on the stove top for a half hour or so. Strain the ingredients, keeping the liquid. Completely cool the liquid before pouring the liquid over your raw meat, submerging it completely. A zip lock bag works very well for this application because you can remove the air from the bag while keeping your refrigerator and the food inside it safe from raw meat spills. Brine from 12 to 24 hours under refrigeration, remove from brine, pat dry, and cook the meat as you would normally. I like to grill or sear hard in an iron skillet. 

               

Per Serving (excluding unknown items): 377 Calories; 30g Fat (64.0% calories from fat); 4g Protein; 34g Carbohydrate; 4g Dietary Fiber; 96mg Cholesterol; 3087mg Sodium.  Exchanges: 1 1/2 Grain(Starch); 0 Vegetable; 1 Fat; 1/2 Other Carbohydrates.
