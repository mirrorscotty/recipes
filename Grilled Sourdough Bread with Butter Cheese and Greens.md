Grilled Sourdough Bread with Homemade Butter Cheese and Greens
==============================================================

Yield: Serving 4-6 people

Prep Time: 10 minutes

Grilled Sourdough
-----------------
* 1 ea Sourdough Boule from Madison Sourdough cut into slices 
* 2 Tbsp Olive oil
* Pinch of salt

Butter Cheese
-------------
* 1 lb Petite Nuage Cheese from Landmark Creamery, at room temp (or sub Farmer’s Cheese or Cream Cheese)
* ¼ cup cream
* 1 clove garlic, minced
* 1 shallot, minced
* ½ tsp Esplette Pepper
* 1 cup grated Pecora Nocciola from Landmark Creamery (or sub Parmesan)
* ¼ cup tarragon, finely chopped
* 2 tsp fresh thyme leaves, finely chopped
* ¼ cup fresh basil, chopped
* 1 ea lemon; zest and juice
* 3 Tbsp butter, at room temp
* Salt and pepper to taste

Late Summer Green Salad
-----------------------
* ½ lb Farmhouse Mix from Garden to Be
* 1 ea lemon, juiced
* 1 Tbsp Olive oil
* Salt to taste

Directions
----------
Whip the Farmer’s Cheese and Cream in a food processor until smooth.  Add the remaining ingredients and pulse a few times to combine.  Adjust seasoning with salt and pepper.
Brush the sourdough bread slices with olive oil and season with a little salt.  Grill or toast lightly.
Toss salad greens with lemon juice, olive oil and a little salt.
Spread a layer of butter cheese on the grilled sourdough.   Top with salad greens and enjoy the final days of summer.

Source: Epic