Gluten-Free Vegan Matcha Strawberry Sponge Cake
===============================================
Ingredients
-----------
* 8 tablespoons coconut oil (or sub olive or vegetable oil)
* 230 ml (1 cup) milk, any kind
* Juice of 1 lemon\*
* 8 tablespoons maple syrup (or sub any other sweetener)
* 1 teaspoon vanilla extract
* Pinch of salt
* 150 g (1 1/4 cup) ground almonds
* 150 g (1 1/4 cup) gluten-free flour (or sub plain flour if not gluten-free)
* 2 heaped teaspoons baking power (ensure gluten-free if necessary)
* 1/4 teaspoon bicarbonate of soda
* 1-2 teaspoons matcha green tea powder, to taste

#For the strawberry frosting
* 400 g (14oz tin) coconut milk\*\*
* 2 tablespoons strawberry jam (refined sugar free if necessary)

#To decorate
* 200 g (7oz) fresh berries, chopped into smaller pieces if necessary - I used strawberries, blackberries and cherries
* Few sprigs of fresh mint

Instructions
------------
#For the cake
1. Preheat oven to 180 degrees (350 degrees)
2. Place the coconut oil in a large bowl and melt over a saucepan of boiling water or in the microwave (skip this step if using any other oil)
3. Once melted, add the milk to the same bowl along with the lemon juice, maple syrup, vanilla, salt and ground almonds
4. Sift in the flour, baking powder, bicarbonate of soda and matcha powder
5. Mix well, adding a tiny splash more milk if it's looking too dry
6. Transfer mixture between two small greased baking tins lined with baking paper (I used two 7inch/18cm tins)
7. Bake in oven for around 15 minutes until risen and an inserted skewer comes out clean
8. Once out the oven, immediately remove cakes from baking tins and transfer onto a wire rack to cool

#For the strawberry frosting
1. You'll need to keep your tin of coconut milk in the fridge overnight for the rich creamy bit to separate from the watery bit at the bottom. Make sure it is full fat coconut milk, otherwise this separation won't happen and you won't be able to make this cream!
2. Open the tin upside down - the coconut cream will be at the bottom and you can just pour off the watery liquid (save this for adding to soups or smoothies)
3. Place the thick, creamy part into a bowl
4. Add the strawberry jam
5. Use an electric whisk (or a manual one and a lot of elbow grease) to whip up the coconut cream until light and fluffy

#To decorate
1. Once the sponges have cooled completely, place one sponge on a large plate or cake stand, and cover with half of the cream
2. Spread the strawberry frosting over this layer of cream and add half of the berries
3. Place other sponge on top and use remaining frosting to cover the top
4. Cover with the remaining berries
5. Best eaten on the day or day after it's made

Recipe Notes
------------
\* The lemon juice is crucial as its acidity needs to react with the alkali bicarbonate of soda to make the sponge fluffy. If you don't want to use lemon juice, substitute with 1 tablespoon vinegar instead
\*\* You can alternatively use 200g (7oz) thick coconut yogurt
