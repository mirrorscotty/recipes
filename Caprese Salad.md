Caprese Salad
=============
Yield: Serving 4-6 people
Prep Time: 10 minutes

Ingredients
-----------
* 2 lbs Heirloom Tomatoes, use an assortment of varieties - cut into ¼ inch slices 
* 1 lb Fresh Mozzarella Petit Nuage – sliced into ¼ inch slices or torn into ½ inch chunks
* ½ lb Basil – Dark Opal Purple and Italian Genovese
* 2 Tablespoons  High Quality Extra Virgin Olive Oil 
* 2 Tablespoons Course Sea Salt 
* 1 Table Fresh Cracked Black Pepper – Coarsely Cracked

Directions
----------
Shingle the slices of tomatoes out on a platter. Tear the mozzarella or petite nuage into bite size pieces and sprinkle on the tomatoes [optionally, slice the mozzarella and layer it between the tomatoes]. 
Gently tear the basil and sprinkle all over the tomatoes. 
Just before serving sprinkle the salad with salt/cracked pepper and drizzle all with olive oil. 
Serve with a baguette or bread of choice and enjoy!!

Source: Epic