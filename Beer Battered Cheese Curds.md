Beer Battered Cheese Curds
==========================
Equipment
---------
* 1 deep fryer, heavy bottomed stock pot, or large pot
* 1 cooking thermometer (if using a pot)
* 2 mixing bowls
* 1 strainer
* 1 slotted spoon or a spider if you have one
* 1 large plate or bowl lined with paper towels

Recipe
======
Curds
-----
* 2.5 pounds fresh cheese curds- your favorite flavor will do
* 1 cup all-purpose flour, for dredging
* 3 quarts Canola oil

Batter
------
* 1/3 cup whole milk 
* 1.5 cups flour 
* 1 cup local beer, in this case I’m using Ale Asylum’s Ambergeddon
* 2 teaspoons salt
* 3 large eggs

Method
------
* Mix all the beer batter ingredients together, and place it on ice, so it stays very cold. 
* Place the cheese curds in the batter. Working in batches, strain slightly, and drop them into the fryer, taking care not to overfill so the oil doesn't boil over. As you do so, the oil will drop in temperature. You are looking for a steady 350 degrees after you place your cheese curds in the oil.
* Fry to a golden brown. 
* Remove cheese curds with a spider or a slotted spoon, and place on a paper towel-lined plate.
* Serve with your favorite dipping sauce.




