Grilled Thai Beef Salad
=======================

Ingredients
-----------
* 1 pound top-round London broil or flank steak, about 1 to 1 1/2-inches thick
* 3 tablespoons lime juice, divided
* 3 tablespoons low-sodium soy sauce
* 3 tablespoons canola oil
* 2 tablespoon brown sugar
* 1 teaspoon minced garlic
* 1 1/2 teaspoons minced ginger
* 1 1/4 teaspoons red curry paste or chili-garlic sauce
* 1/2 head red-leaf lettuce, torn (about 6 cups)
* 3 shallots, thinly sliced (about 1/2 cup), divided, for garnish
* 1/2 cup cilantro leaves, rinsed and dried
* 1 cup basil leaves, sliced into ribbons

Directions
----------
1.  Rinse and pat the meat dry. Place in a sealable plastic bag or small glass
    dish. In a medium bowl combine 1 tablespoon of the lime juice, soy sauce,
    canola oil, brown sugar, garlic, ginger and red curry paste. Pour half the
    mixture into the bag with the meat. Add the remaining 2 tablespoons lime
    juice to the bag. Seal tightly, and marinate meat in refrigerator at least
    4 hours or overnight, turning occasionally. Reserve the rest of the mixture
    refrigerated, to dress the salad.

2. Spray grill or grill pan with cooking spray and preheat. Grill steak until
   medium-rare, about 5 minutes per side, depending on desired doneness. Let
   rest until room temperature then slice thinly against the grain.

3. Combine lettuce, sliced shallot, cilantro, basil and beef in a salad bowl,
   reserving a few shallots for garnish. Add the reserved dressing and toss to
   coat. Divide salad among 4 plates and garnish with reserved sliced shallots.

Source: http://www.foodnetwork.com/recipes/ellie-krieger/grilled-thai-beef-salad-recipe-1947960
