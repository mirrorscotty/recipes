Pea Shoot Pesto
3 cups pea shoots, woody stems removed
1 1⁄2 cups packed mint leaves, roughly chopped
1⁄2 cup raw walnuts
1 1⁄2 tbsp. fresh lemon juice
3 garlic cloves, roughly chopped 
1⁄2 cup olive oil
Kosher or sea salt to taste

Bring 2 quarts of water to a boil and have a large bowl of ice water close by.  When the water is boiling drop the pea shoots in, stir and then use a slotted spoon to immediately transfer them to the ice bath.  Drain them and squeeze out the excess water.  Add the shoots, mint, walnuts and garlic to a food processor.  Pulse a few times, add the lemon juice and some salt.  With the machine running, slowly pour in the olive oil.  Taste and adjust the salt or lemon juice to your liking.  This will keep in your refrigerator for a few days or can be frozen for later use.

Source: Epic Culinary