Eggplant Caponata
=================
Ingredients
-----------
* 1 ½ pounds eggplant (1 large or 2 if smaller)
* Olive oil
* 1 medium onion - diced
* 4 cloves garlic – minced
* 1 large red bell pepper – seeded and diced
* 1 pound tomatoes – seeded and diced
* ¼ cup capers, rinsed and drained
* 3 tablespoons pitted green olives – coarsely chopped
* 3 tablespoons red wine vinegar
* 1 tablespoon sugar, or a bit more to taste
* Salt and Pepper to taste
* 2 tablespoons pine nuts - toasted
* 2 tablespoons parsley – minced

Method
------
* Preheat oven to 400 degrees. Cut the eggplant in half length-wise and rub all surfaces with olive oil. Place cut side down on a parchment-lined sheet pan and roast for 45-50 minutes. Eggplant should be very tender when done, giving no resistance when pierced with a knife of fork. Remove from pan and allow to cool. Chop coarsely and reserve.
* In medium pan, start 1-2T olive oil over medium heat. Add onion and a pinch of salt and cook, stirring occasionally, until softened – about 5 minutes. Add garlic, cook for a minute to become fragrant (this could be a good step to add chili flake if you like a bit of heat), then add bell pepper, tomatoes, and another pinch of salt. Cook this mixture, stirring, for another 10 minutes.
* Turn heat to medium-low and add the chopped eggplant, capers, olives, vinegar, and sugar. Cook, stirring frequently, until liquid has reduced and the mixture has thickened. Season to taste with salt and black pepper (and sugar if it needs more). Remove from the heat and allow to cool completely. This is great to chill overnight, covered, if possible.
* When ready to serve, let come to room temperature and top with the toasted pine nuts and parsley. Serve with pita or toasted baguette.

Source: Epic