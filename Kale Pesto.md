Kale Pesto
==========
Ingredients
-----------
* 1 bunch lacinato (dinosaur) kale, center ribs removed and discarded
* 1/3 cup pine nuts or almonds, toasted
* 2 large garlic cloves, roughly chopped
* 1/2 cup extra-virgin olive oil
* 3/4 teaspoon kosher salt 
* 1 lemon, zested and juiced

Directions
----------
1. Bring a large pot of salted water to a boil; fill a bowl with ice water. Drop kale into the boiling water and cook for 1 minute; then, using tongs or a slotted spoon, transfer to the ice water to stop the cooking process. (This is called blanching; it makes greens softer, and can also reduce bitterness.)
2. Drain the kale well, then wrap tightly in a dry kitchen towel and squeeze to remove any excess moisture.
3. Place the kale into a food processor with the pine nuts, garlic, salt and lemon zest, and pulse until the mixture is smooth. With the motor running, slowly drizzle in the oil until fully incorporated. Taste, and add more salt and some lemon juice if necessary.
