Air Fryer Cheese Curds
======================
* 8oz Tom and Sally Murphy’s fresh cheese curds
* ¼ cup All-Purpose flour
* 1 teaspoon sweet paprika
* 1 teaspoon garlic powder
* Salt and pepper
* 2 eggs
* 1 ½ cups panko breadcrumbs
* Cooking spray

Preheat air fryer to 350F. Line a baking tray with wax paper or parchment for easier cleanup.

Prepare your breading stations. Combine flour and seasonings in a medium bowl; these can be customized – if using flavored curds you can season to match (salt and pepper still recommended). Crack and whisk eggs in another medium bowl. Place your panko breadcrumbs in your final bowl.

Bread the curds by starting with the flour dredge – lightly toss and shake off any excess flour. Now into the egg – removing excess once more. Finally, drop into the panko mixture, being sure to get a good coating here. Place your breaded curds onto your prepared baking tray. Repeat until all the curds are breaded. 

Place a single layer of similar-sized curds in the basket of your air fryer and lightly spritz with cooking spray. Cook for 2-3 minutes on one side. Flip the curds over and cook another 3-4 minutes until lightly browned and cheese is warmed through. Repeat until all curds are cooked.

Stone Ground Mustard Dipping Sauce
==================================
* 2 Tablespoons stone ground mustard
* 2 Tablespoons mayonnaise
* 2 teaspoons Bee Charmer honey
* 2 teaspoons Apple cider vinegar
* Salt, to taste

Combine ingredients in a small bowl and check seasoning. Serve alongside curds and enjoy!
