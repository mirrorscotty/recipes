#Ingredients
* 3/4 C sugar
* 3 T cornstarch
* 3 C sliced fresh rhubarb
* 1-1/2 C quick cooking OR old fashioned oats
* 3/4C brown sugar
* 3/4 C butter, melted
* 1/2 C all-purpose flour
* 1-1/2 t ground cinnamon

#Directions
1. In a large bowl, combine sugar and cornstarch.  Add rhubarb, toss to coat.  Spoon into an 8 in. square baking dish.
2. In a small bowl, combine the oats, brown sugar, melted butter, flour and cinnamon until the mixture resembles coarse crumbs.  Sprinkle over the fruit.  Bake at 350 for 45 minutes or until bubbly and fruit is tender.

Source: Epic Culinary