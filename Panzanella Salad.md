Panzanella Salad
================
* 1 baguette or other hearty bread
* 1 clove garlic, peeled and cut in half
* 4 ripe Heirloom tomatoes
* ½ cup diced red onion
* 1 ounce fresh mozzarella cut into ¼” cubes (optional)
* 10 fresh basil leaves, finely chopped
* 2 cloves garlic, minced
* 2 tablespoons balsamic vinegar
* 3 tablespoons extra virgin olive oil
* Salt and freshly ground black pepper

Directions
==========
* Slice bread into ½ inch slices. Grill on both sides. Rub the halved garlic clove over the still warm bread and let cool. Cut the slices into ½” cubes.
* Core and chunk up the tomatoes. Combine in a large bowl with the onion, cheese (if using), basil and garlic. Toss to combine.
* Just before serving, add the bread cubes and toss gently. Let sit for five minutes.
* Whisk together the vinegar and olive oil and drizzle over tomato mixture. Toss and season to taste with salt and pepper. Serve immediately. 

Serves 6

