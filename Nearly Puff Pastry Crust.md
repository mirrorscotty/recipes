Nearly Puff Pastry Crust
========================
Ingredients
-----------
1-1/4 cup amaranth flour
1/2 cup rice flour
3/4 cup tapioca starch
1 tsp xanthan gum
1 tbsp sugar
1 tsp salt
1-1/2 tsp baking powder
12 tbsp (1.5 sticks) cold, unsalted butter cut into small pieces
2 large eggs
1 tbsp water

Directions
----------
In the bowl of a food processor, combine the flours, xanthan gum, sugar, salt,
and baking powder. Add the butter and pulse until the mixture resembles
coarse meal. Add the eggs and water and pulse to combine. Remove to a work
surface and knead into a ball.

Source: Gluten-Free Makeovers (book, page 98), Grandma
