Pesto Potato Salad
==================
Salad
-----
* 1.5 pounds potatoes
* ½ sweet white onion, diced small
* ½ cup sunflower seeds
* 1/3 cup basil pesto, from below
* Salt and pepper to taste

Pesto
-----
* ¼ cup sunflower seeds
* 1 clove garlic
* ¼ cup Landmark Creamery’s Pecora Nocciola, grated
* 3 cups basil, loosely packed
* 1 tablespoon fresh lemon juice
* ¼ cup olive oil
* Salt and pepper to taste

Directions
----------
Steam the potatoes until tender – a fork should easily pierce the skin. Cool completely. Combine with remaining ingredients, seasoning with salt and pepper to taste.
Pesto: Toast the sunflower seeds in a dry skillet for 1-2 minutes until aromatic. Allow to cool to room temp. Process seeds with garlic in the food processor until smooth. Add the basil and lemon juice and process until basil breaks down a bit. With the processor running, slowly add the olive oil until well blended. Season to taste with salt and pepper.
