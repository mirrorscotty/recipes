Height of Summer Vegetables with Landmark Creamery Sheep’s Milk Cheese and Underground Sausage
==============================================================================================

Ingredients
-----------
* Half loaf of Epic’s French bread (available in General Store and Tokyo Metro Monday, Wednesday and Friday), cut into about 1” cubes
* 1/2 c. extra virgin olive oil
* Pecora Nocciola or Anabasque cheese from Landmark Creamery, grated
* A variety of tomatoes, either sliced or chunked, lightly salted
* 1 small cucumber, cut in half and sliced in about 1/2" slices
* 1 red pepper, cored and seeded, cut into about 1" squares
* 4 ounces Underground Spanish chorizo or Ghost Pepperoni, peeled and sliced pretty thin

Directions
----------
1. Prep veggies, cheese and sausage, place in a large serving bowl
2. In a large sauté pan over medium heat, toast the bread cubes, turning as they brown a bit on each side. 
3. Once bread is toasted, toss with other ingredients. Season with salt and pepper. Drizzle with additional olive oil if desired. 
