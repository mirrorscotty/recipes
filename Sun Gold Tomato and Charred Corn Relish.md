Sun Gold Tomato and Charred Sweet Corn Corn Relish
==================================================
(delicious on grilled chicken, pork or fish)

Ingredients
-----------
* 3 ears sweet corn, shucked, silk removed
* 1 pound of cherry tomatoes, Sun Gold variety if available, washed and quartered
* 1 red or green pepper, washed, core and stem removed, diced fairly small
* 1 T. fresh chopped thyme (definitely NOT dry thyme)
* 1 t. red wine vinegar
* 2 T. extra virgin olive oil (optional)
* Salt and fresh ground black pepper to taste

Method
------
Char the corn by placing directly over a (gas) burner on your range set on medium high.
Allow kernels to char slightly before turning.
Continue to rotate corn with a tongs until all sides are charred (not all the kernels need be charred).
Remove from heat and allow to cool to room temperature.
Meanwhile prep remaining ingredients and place in mixing bowl.
When corn is cooled off, remove from cob with a sharp knife and add to the bowl.
Mix together and adjust salt and pepper to taste.
Serve room temperature. 

Notes/options
-------------
* Any ripe cherry tomato will do
* A variety of colors of cherry tomatoes looks and tastes great
* May substitute lime or lemon juice for the vinegar
* A pinch or two of smoked paprika adds a nice touch
* Also very good on scrambled eggs


