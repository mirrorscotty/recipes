Cherry Almond Tart
==================
For the crust
-------------
* 1½-cups Almond Meal
* 1-tablespoon sugar
* ¼-teaspoon sea salt
* ¼-teaspoon ground cinnamon
* 5-tablespoons butter 

Cherry Ricotta Filling
----------------------
* 1-cup Ricotta Cheese
* ¼-cup pitted cherries, smashed
* 2-teaspoons Almond Extract
* ¼-teaspoon sea salt
* 1-tablespoon honey
* Pitted and quartered cherries for top
* ¼-cup chopped pistachios for top

Directions
----------
1.	Preheat oven to 350 degrees F with rack in the middle.
2.	Prepare the crust. Whisk almond meal, sugar, cinnamon, and sea salt together in a bowl, breaking apart any lumps. Add butter, and use hands to mix until dough comes together. Press into 9” tart pan, pressing evenly and all the way up the sides. Place on baking sheet and bake for 10 minutes. Remove from oven and cool.
3.	Prepare the filling. Mix ricotta, smashed cherries, extract, honey, and salt. Once crust has completely cooled, evenly distribute filling onto crust. Smooth with back of spoon or offset spatula. 
4.	Top with quartered cherries and chopped pistachios
5.	Place tart (on baking sheet) back in oven for 10 minutes. Finish by broiling on low for 4-5 minutes until edges of crust are brown. Watch carefully so it does not burn.
